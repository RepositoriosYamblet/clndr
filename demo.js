// Call this from the developer console and you can control both instances
var calendars = {};

var fecha = 0;

$(document).ready(function() {

    console.info(
        'Welcome to the CLNDR demo. Click around on the calendars and' +
        'the console will log different events that fire.');

    // Assuming you've got the appropriate language files,
    // clndr will respect whatever moment's language is set to.
    // moment.locale('ru');

    // Here's some magic to make sure the dates are happening this month.
    var thisMonth = moment().format('YYYY-MM');
    // Events to load into calendar
    var eventArray = [];

    // The order of the click handlers is predictable. Direct click action
    // callbacks come first: click, nextMonth, previousMonth, nextYear,
    // previousYear, nextInterval, previousInterval, or today. Then
    // onMonthChange (if the month changed), inIntervalChange if the interval
    // has changed, and finally onYearChange (if the year changed).


    // Calendar 3 renders two months at a time, paging 1 month
    var adelante = document.getElementsByClassName("clndr-next-button")
    var atras = document.getElementsByClassName("clndr-previous-button")
    calendars.clndr3 = $('.cal3').clndr({
        lengthOfTime: {
            months: 1,
            interval: 1
        },
        daysOfTheWeek: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        showAdjacentMonths: false,
        adjacentDaysChangeMonth: false,
        events: eventArray,
        multiDayEvents: {
            endDate: 'endDate',
            startDate: 'startDate'
        },
        clickEvents: {
            click: function(target) {
                console.log('Cal-3 clicked: ', target);

                fecha = target; //pasa los datos del dia seleccionado

                console.log(fecha.date._i); //imprime solo la fecha en formato AAAA-MM-DD
                var arregloFecha = fecha.date._i.split("-") //divide el string de fecha en 3 
                var dia = arregloFecha[2]; //guarda el valor del dia
                var mes = arregloFecha[1]; //guarda el valor del mes
                if (parseInt(dia) >= parseInt(moment().format('DD'))) {
                    console.log("hoy es " + dia)
                    window.open("envia.php?mes=" + mes + "&dia=" + dia); //abre una nueva pestaña con el link creado
                } else {
                    console.log("dia invalido");
                }
            },
            nextInterval: function() {
                console.log('Cal-3 next interval');
                adelante[0].remove();
            },
            previousInterval: function() {
                console.log('Cal-3 previous interval');
                atras[0].remove();
            },
            onIntervalChange: function() {
                console.log('Cal-3 interval changed');
                cambiarCursor();
            },
            today: function(month) {
                atras[0].remove();
            }
        },
        template: $('#template-calendar-months').html()
    });


    cambiarCursor();
    atras[0].remove();
    // Bind all clndrs to the left and right arrow keys
    $(document).keydown(function(e) {
        // Left arrow
        if (e.keyCode == 37) {
            calendars.clndr1.back();
            calendars.clndr2.back();
            calendars.clndr3.back();
        }

        // Right arrow
        if (e.keyCode == 39) {
            calendars.clndr1.forward();
            calendars.clndr2.forward();
            calendars.clndr3.forward();
        }
    });
});


var vacios = document.getElementsByClassName("empty last-month");
var dias = document.getElementsByClassName("day");

function cambiarCursor() {

    for (let index = 0; index < dias.length; index++) {

        dias[index].style = "cursor:pointer";
    }
}